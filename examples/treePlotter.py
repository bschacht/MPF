#!/usr/bin/env python

import os

import ROOT
from MPF.examples import exampleHelpers
from MPF.treePlotter import TreePlotter
from MPF.process import Process

testTreePath = "/tmp/testTreeMPF.root"

if not os.path.exists(testTreePath):
    exampleHelpers.createExampleTrees(testTreePath)

# Default Options can already be set when the plotter is initialized
tp = TreePlotter(cut="ht>1000", varexp="met", xTitle="E_{T}^{miss}", unit="GeV", plotType="DataMCRatioPlot")

# Processes and their corresponding trees can be added via addProcessTree
tp.addProcessTree("Bkg 2", testTreePath, "w2", style="background")
tp.addProcessTree("Signal", testTreePath, "w4", style="signal")

# Processes can have custom cuts that are only applied for them
# Here we use this to scatter the background a bit, so our "data"
# which will be in the end from the same tree looks a bit more
# different ;)
tp.addProcessTree("Bkg 1", testTreePath, "w1", cut="(1-50*(1-w))", style="background")

# If multiple trees/files correspond to one process then you can
# explicitely create the Process object and add it to the plotter
data = Process("data", style="data")
data.addTree(testTreePath, "w1")
data.addTree(testTreePath, "w2")
tp.addProcess(data)

# options given to plot are only valid for the particular plot (and temporarily can overwrite the defaults)
tp.plot("plot.pdf", xmin=0., xmax=3000, nbins=100, ratioUp=1.25, ratioDown=0.75)


#!/usr/bin/env python
"""
Standard plot, showing a stack of several background
histograms, and/or overlayed signals and data points

.. _plotExample:

Example
--------

.. literalinclude:: /../examples/plot.py

.. image::  images/plot.png
   :width: 600px

"""

from .plotStore import PlotStore

class Plot(PlotStore):

    """
    .. inheritance-diagram:: MPF.plot.Plot

    For further options see :py:meth:`~MPF.plotStore.PlotStore`
    """

    def __init__(self, **kwargs):
        super(Plot, self).__init__(**kwargs)

    def saveAs(self, path, **kwargs):
        self.buildMainPad(logy=kwargs.pop("logy", False))
        return super(Plot, self).saveAs(path, **kwargs)

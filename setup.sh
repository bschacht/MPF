thisfile=${BASH_SOURCE[0]}
if [[ -n "$ZSH_NAME" ]]
then
    thisfile=${(%):-%N}
fi

MPFPATH=$(dirname $(readlink -f -- ${thisfile}))
pythonpath=${MPFPATH}/pythonpath

[[ -d ${pythonpath} ]] || mkdir ${pythonpath}
[[ -h ${pythonpath}/MPF ]] && rm ${pythonpath}/MPF
ln -s ${MPFPATH} ${MPFPATH}/pythonpath/MPF

export PYTHONPATH=$pythonpath:$PYTHONPATH

echo "$pythonpath added to PYTHONPATH"
echo "To have MPF always setup add the following to your ~/.bashrc:"
echo "export PYTHONPATH=$pythonpath:\$PYTHONPATH"

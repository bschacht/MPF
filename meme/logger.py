"""
Logger for meme
"""

import logging

# create logger
logger = logging.getLogger("meme")

# add a null handler so programs can run, even if they don't setup logging
logger.addHandler(logging.NullHandler())

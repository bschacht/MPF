#!/usr/bin/env/python
"""
Similiar to :py:meth:`~MPF.plot`, but also shows a ratio of
data and the total background in a bottom pad.

Example
--------

.. literalinclude:: /../examples/dataMCRatioPlot.py

.. image::  images/dataMCRatioPlot.png
   :width: 600px

"""
from .plotStore import PlotStore
from . import pyrootHelpers as PH
from .line import Line
from .errorBands import AE
from . import globalStyle as gst
import ROOT
from .commonHelpers.logger import logger
logger = logger.getChild(__name__)

class DataMCRatioPlot(PlotStore):

    """
    Overwrites the defaults for the following :py:meth:`~MPF.plotStore.PlotStore` parameters:

    :param ignoreNumErrors: default: False
    :param ignoreDenErrors: default: True
    :param ratioMode: default: "rawpois"

    For further options see :py:meth:`~MPF.plotStore.PlotStore`

    """

    def __init__(self, **kwargs):
        super(DataMCRatioPlot, self).__init__(splitting='ratio',
                                              ignoreNumErrors=kwargs.pop("ignoreNumErrors", False),
                                              ignoreDenErrors=kwargs.pop("ignoreDenErrors", True),
                                              ratioMode=kwargs.pop("ratioMode", "rawpois"),
                                              **kwargs)

    def saveAs(self, path, **kwargs):
        self.buildMainPad(**kwargs)

        bottomPad = self.canvas.pads['bottom']
        self.addDataMCRatio(bottomPad)

        return super(DataMCRatioPlot, self).saveAs(path, **kwargs)

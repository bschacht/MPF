# should use at least python 2.7
import sys, os
assert sys.version_info >= (2,7), "MPF needs at least python 2.7 - if you are in an slc environment consider using 'lsetup python'"

# unfortunately i have to do this here
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

# user shouldn't use ROOT5
from .commonHelpers.logger import logger
logger = logger.getChild(__name__)

if not "BUILDINGSPHINX" in os.environ:
    if int(ROOT.gROOT.GetVersion().split(".")[0]) < 6:
        logger.warning("You are using ROOT version {}, ROOT version 6 is recommended".format(ROOT.gROOT.GetVersion()))

# import some classes into the global namespace
from .plot import Plot
from .dataMCRatioPlot import DataMCRatioPlot
from .bgContributionPlot import BGContributionPlot
from .significanceScanPlot import SignificanceScanPlot
from .signalRatioPlot import SignalRatioPlot
from .treePlotter import TreePlotter

from . import pyrootHelpers as PH

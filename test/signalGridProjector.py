import unittest
import os
import json

from .. import meme

from ..examples.exampleHelpers import createExampleSignalGrid, createExampleTrees
from .. import IOHelpers as IO
from ..signalGridProjector import SignalGridProjector
from . import mhd
from ..histProjector import HistProjector
from ..commonHelpers.options import checkUpdateOpt, checkUpdateDict

from .helpers.floatTester import FloatTester

from ..commonHelpers.logger import logger
logger = logger.getChild(__name__)

class Test(FloatTester):

    testTreePath = "/tmp/testTreeMPF.root"
    testSignalPath = "/tmp/testSignalGrid.root"
    m1range = (1000, 2000, 100)
    m2range = (1000, 2000, 100)
    nevents = 1000

    def assertEqualHarvestList(self, list1, list2, sortkey1="m1", sortkey2="m2"):
        for pointDictNew, pointDictOld in zip(sorted(list1, key=lambda x: (x[sortkey1], x[sortkey2])),
                                              sorted(list2, key=lambda x: (x[sortkey1], x[sortkey2]))):
            for key in pointDictNew:
                self.assertClose(float(pointDictNew[key]), float(pointDictOld[key]))

    @classmethod
    def setUpClass(cls):
        if not os.path.exists(cls.testSignalPath):
            logger.info("creating example tree {}".format(cls.testSignalPath))
            createExampleSignalGrid(cls.testSignalPath,
                                    nevents=cls.nevents,
                                    m1range=cls.m1range,
                                    m2range=cls.m2range)
        if not os.path.exists(cls.testTreePath):
            logger.info("creating example tree {}".format(cls.testTreePath))
            createExampleTrees(cls.testTreePath)


    def test_signalGridCreated(self):
        """
        Test if signal grid was created with the desired options
        """
        with IO.ROpen(self.testSignalPath) as rf:
            if len(rf.GetListOfKeys()) < 1:
                self.fail("No trees found")
            for k in rf.GetListOfKeys():
                tree = rf.Get(k.GetName())
                logger.debug("found tree {} with {} entries".format(tree.GetName(), tree.GetEntries()))
                self.assertEqual(int(tree.GetEntries()), self.nevents)


    def test_getSignalPointDict(self):
        pointDict = SignalGridProjector.getSignalPoints("w_(?P<m1>.*?)_(?P<m2>.*)$", self.testSignalPath)
        if len(pointDict) < 1:
            self.fail("Point dict empty")
        for pointTree, paramDict in pointDict.items():
            logger.debug("Matched dict for {} : {}".format(pointTree, paramDict))
            m1 = pointTree.split("_")[1]
            m2 = pointTree.split("_")[2]
            self.assertEqual(m1, paramDict["m1"])
            self.assertEqual(m2, paramDict["m2"])


    def test_totalBackgroundHist(self):
        hp = HistProjector()
        hphist = hp.getTH1PathTrees(
            [
                (self.testTreePath, "w1"),
                (self.testTreePath, "w2"),
                (self.testTreePath, "w3"),
            ], "1")
        sp = SignalGridProjector(cut="1", weight="1")
        sp.addProcessTree("w1", self.testTreePath, "w1", style="background")
        sp.addProcessTree("w2", self.testTreePath, "w2", style="background")
        sp.addProcessTree("w3", self.testTreePath, "w3", style="background")
        sp.fillHists(checkUpdateOpt(sp.defaults))
        sphist = sp.getTotalBkgHist()
        self.assertEqualHists(hphist, sphist)


    def test_getHarvestList(self):
        sp = SignalGridProjector(cut="mt>1000&&met>1000", weight="0.005")
        sp.addProcessTree("w1", self.testTreePath, "w1", style="background")
        sp.addProcessTree("w2", self.testTreePath, "w2", style="background", cut="0.1")
        sp.addProcessTree("w3", self.testTreePath, "w3", style="background", cut="0.01")
        sp.addSignalProcessesByRegex("w_(?P<m1>.*?)_(?P<m2>.*)$", "w grid", self.testSignalPath, cut="20")
        harvest = sp.getHarvestList("w grid")

        # dump in case new reference is needed
        newreference = "/tmp/test_getHarvestList_reference.json"
        with open(newreference, "w") as of:
            logger.debug("dumping new reference to {}".format(newreference))
            json.dump(harvest, of)

        # compare to old reference (ToDo: Is this the right way of loading the file?)
        oldreference = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                    "references/test_getHarvestList_reference.json")
        with open(oldreference) as f:
            oldharvest = json.load(f)
        self.assertEqualHarvestList(harvest, oldharvest)


    def test_registerHarvestList(self):
        previousRefresh = meme.getOption("refreshCache")
        meme.setOptions(refreshCache=True)
        sp = SignalGridProjector(cut="mt>1000&&met>1000", weight="0.005")
        sp.addProcessTree("w1", self.testTreePath, "w1", style="background")
        sp.addProcessTree("w2", self.testTreePath, "w2", style="background", cut="0.1")
        sp.addProcessTree("w3", self.testTreePath, "w3", style="background", cut="0.01")
        sp.addSignalProcessesByRegex("w_(?P<m1>.*?)_(?P<m2>.*)$", "w grid", self.testSignalPath, cut="20")
        harvest = sp.getHarvestList("w grid")
        sp.registerHarvestList("w grid", "highmt_highmet", "background")
        harvestListDict = sp.getAllHarvestLists(useMultiHistDraw=True)
        self.assertEqualHarvestList(harvest, harvestListDict["w grid"]["highmt_highmet"])
        meme.setOptions(refreshCache=previousRefresh)

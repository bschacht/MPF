MPF package
===========

Submodules
----------

.. toctree::

   MPF.IOHelpers
   MPF.arrow
   MPF.atlasStyle
   MPF.bgContributionPlot
   MPF.canvas
   MPF.dataMCRatioPlot
   MPF.errorBands
   MPF.globalStyle
   MPF.histProjector
   MPF.histograms
   MPF.labels
   MPF.legend
   MPF.line
   MPF.multiHistDrawer
   MPF.pad
   MPF.plot
   MPF.plotStore
   MPF.process
   MPF.processProjector
   MPF.pyrootHelpers
   MPF.signalGridProjector
   MPF.signalRatioPlot
   MPF.significanceScanPlot
   MPF.efficiencyPlot
   MPF.treePlotter
   MPF.n1plotter

Subpackages
-----------

.. toctree::

    MPF.commonHelpers
    MPF.examples

Module contents
---------------

.. automodule:: MPF
    :members:
    :undoc-members:
    :show-inheritance:

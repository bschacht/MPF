MPF.canvas module
=================

.. automodule:: MPF.canvas
    :members:
    :undoc-members:
    :show-inheritance:

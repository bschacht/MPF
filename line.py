import math
import ROOT

from .commonHelpers.logger import logger
logger = logger.getChild(__name__)

from . import globalStyle as gst

class Line(ROOT.TLine):
    def __init__(self, *args, **kwargs):
        self.zIndex = 2
        self.lineStyle = kwargs.pop("lineStyle", gst.TLineStyle)
        self.lineWidth = kwargs.pop("lineWidth", gst.TLineWidth)
        self.lineColor = kwargs.pop("lineColor", gst.TLineColor)
        if kwargs:
            raise Exception("Got unexpected kwargs: {}".format(kwargs))
        super(Line, self).__init__(*args)

    def draw(self, **kwargs):
        self.SetLineStyle(self.lineStyle)
        self.SetLineWidth(self.lineWidth)
        self.SetLineColor(self.lineColor)
        self.Draw()

class CutLine(Line):
    def __init__(self, *args, **kwargs):
        self.direction = kwargs.pop("direction", "right")
        self.height = kwargs.pop("height", gst.cutLineHeight)
        self.minimum = None
        self.maximum = None
        self.logy = False
        super(CutLine, self).__init__(*args,
                                      lineStyle=kwargs.pop("lineStyle", gst.cutLineStyle),
                                      lineWidth=kwargs.pop("lineWidth", gst.cutLineWidth),
                                      lineColor=kwargs.pop("lineColor", gst.cutLineColor),
                                      **kwargs)

    def draw(self, **kwargs):
        # the following calculations are to be checked ...
        deltaY = self.maximum - self.minimum
        if self.logy:
            expMin = math.log10(self.minimum)
            deltaExp = math.log10(self.maximum)-math.log10(self.minimum)
        if self.height is not None:
            # recalculate maximum
            if not self.logy:
                self.SetY2(self.minimum+self.height*deltaY)
            else:
                self.SetY1(10**expMin)
                self.SetY2(10**(deltaExp*self.height+expMin))
        if gst.cutLineArrows:
            if not self.logy:
                y = gst.cutLineArrowPos*self.height*deltaY
            else:
                y = 10**(gst.cutLineArrowPos*self.height*(deltaExp)+expMin)
            logger.debug("gPad.GetX1(): {}, gPad.GetX2(): {}".format(ROOT.gPad.GetX1(), ROOT.gPad.GetX2()))
            dx = 0.1*(ROOT.gPad.GetX2()-ROOT.gPad.GetX1())
            x1 = self.GetX1()
            x2 = x1+dx if self.direction == "right" else x1-dx
            self.arrow = ROOT.TArrow(x1, y, x2, y, 0.02, "|>")
            self.arrow.SetLineStyle(self.lineStyle)
            self.arrow.SetLineWidth(self.lineWidth)
            self.arrow.SetLineColor(self.lineColor)
            self.arrow.SetFillColor(self.lineColor)
            self.arrow.Draw()
        self.SetLineStyle(self.lineStyle)
        self.SetLineWidth(self.lineWidth)
        self.SetLineColor(self.lineColor)
        self.Draw()
